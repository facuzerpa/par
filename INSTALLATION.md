**Configure a remote for a fork**

$ git remote -v"

$ git remote add upstream git@bitbucket.org:paradigmas-programacion-famaf/grupo00_lab03_2019.git

$ git remote -v
 
**Syncing to fork**

$ git fetch upstream 

$ git checkout master

$ git merge upstream/master

**Intall NVM**

https://github.com/nvm-sh/nvm

$ nvm install --lts

**Project**

$ npm install --dev

$ npm start

**esLint**

$ npm install --save-dev --save-exact \
eslint \
babel-eslint \
eslint-config-airbnb \
eslint-config-babel \
eslint-config-prettier \
eslint-plugin-import \
eslint-plugin-react \
prettier \
eslint-plugin-prettier \
eslint-plugin-jsx-a11y

$ vim .eslintrc

{
    "extends": [
        "airbnb",
        "plugin:prettier/recommended",
        "plugin:jsx-a11y/recommended"
    ],
    "plugins": [
        "prettier",
        "jsx-a11y"
    ],
    "parser": "babel-eslint",
    "env": {
        "browser": true
    }
}

**React-Icon**

$npm install react-icons --save



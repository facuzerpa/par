import React from "react";

class Header extends React.Component {
  render() {
    return (
      <div className="mb-5">        
      <nav className="navbar navbar-light bg-light">
        <span className="navbar-brand mb-0 h1">MayWeather</span>
      </nav>
      </div>
    );
  } 
}

export default Header;
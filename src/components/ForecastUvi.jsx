import React from 'react';

class ForecastUvi extends React.Component {
    render() {
      return(
        <div className="row w-100 mt-5 m-0">
            <div className="border border-primary w-100">
                <span>Forecast Uvi Index:</span>
            </div>
            <div className="d-flex justify-content-center p-2 border border-primary w-100" >
                <div className="w-25 p-2 border border-primary"><p>Mon</p></div>
                <div className="w-25 p-2 border border-primary"><p>Tue</p></div>
                <div className="w-25 p-2 border border-primary"><p>Wed</p></div>
                <div className="w-25 p-2 border border-primary"><p>Thu</p></div>
                <div className="w-25 p-2 border border-primary"><p>Fri</p></div>
            </div>
        </div>
      );
    }
  }
  
  export default ForecastUvi;
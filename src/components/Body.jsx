import React from "react";
import Header from "./Header";
import TitleForm from "./TitleForm";
import Weather from "./Weather";

class App extends React.Component {
  render() {
    return (
      <div>
        <Header/>
        <TitleForm />
        <Weather />
      </div>
    );
  }
}

export default App;

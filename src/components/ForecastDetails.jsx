import React from 'react';

class ForecastDetails extends React.Component {
    render() {
      return(
        <div className="row collapse" id="collapseDetails">
          <div className="d-flex justify-content-center flex-column p-2 border border-primary w-100" >
            <div className="w-auto m-2 p-2 border border-primary"><p>00:00 - 03:00</p></div>
            <div className="w-auto m-2 p-2 border border-primary"><p>03:00 - 06:00</p></div>
            <div className="w-auto m-2 p-2 border border-primary"><p>06:00 - 09:00</p></div>
            <div className="w-auto m-2 p-2 border border-primary"><p>09:00 - 12:00</p></div>
            <div className="w-auto m-2 p-2 border border-primary"><p>12:00 - 15:00</p></div>
            <div className="w-auto m-2 p-2 border border-primary"><p>15:00 - 18:00</p></div>
            <div className="w-auto m-2 p-2 border border-primary"><p>18:00 - 21:00</p></div>
            <div className="w-auto m-2 p-2 border border-primary"><p>21:00 - 00:00</p></div>
          </div>
        </div>
      );
    }
  }
  
  export default ForecastDetails;
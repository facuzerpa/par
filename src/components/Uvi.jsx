import React from 'react';
import CurrentUvi from "./CurrentUvi";
import ForecastUvi from "./ForecastUvi";
import LastDaysUvi from "./LastDaysUvi";


class Uvi extends React.Component {
    render() {
      return(
        <div className="container w-100 m-0 p-0">
          <CurrentUvi />
          <ForecastUvi />
          <LastDaysUvi />
        </div>
      );
    }
  }
  
  export default Uvi;
import React from "react";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.locationInput = React.createRef();
    this.handleClickForm = this.handleClickForm.bind(this);
  }

  handleClickForm(e) {
    e.preventDefault();
    this.props.handleUpdateLocation(this.locationInput.current.value);
  }

  render() {
    return (
      <div className="form-row">
        <div className="form-group col-md-8 mb-3">
          <input
            type="text"
            ref={this.locationInput}
            className="form-control"
            placeholder="City name, country code"
            required
          />
        </div>
        <button
          onClick={this.handleClickForm}
          type="submit"
          className="btn btn-primary mb-3"
        >
          Check weather
        </button>
      </div>
    );
  }
}

export default Form;

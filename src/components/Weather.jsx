import React from "react";
import axios from "axios";
import Current from "./Current";
import Forecast from "./Forecast";
import Uvi from "./Uvi";

class Weather extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: null,
      forest: null
    };
    this.handleCurrent = this.handleCurrent.bind(this);
    this.handleForest = this.handleForest.bind(this);
  }

  handleCurrent() {
    axios
      .get("http://api.openweathermap.org/data/2.5/weather", {
        params: {
          q: this.props.location,
          appid: "37202fc4687c90288f1f8cfbd99db08e",
          units: 'metric'
        }
      })
      .then(res => {
        this.setState({ current: res.data });
      });
  }

  handleForest() {
    axios
      .get("http://api.openweathermap.org/data/2.5/forecast", {
        params: {
          q: this.props.location,
          appid: "37202fc4687c90288f1f8cfbd99db08e",
          units: 'metric'
        }
      })
      .then(res => {
        this.setState({ forest: res.data.list });
      });
  }

  render() {
    return (
      <div className="container border border-primary" id="accordion">
        <h1>{this.props.location == "" ? "Loading" : this.props.location}</h1>
        <div className="d-flex justify-content-start">
          <button
            className="btn btn-primary m-1"
            data-toggle="collapse"
            data-target="#collapseOne"
            aria-expanded="true"
            aria-controls="collapseOne"
            onClick={this.handleCurrent}
          >
            Current
          </button>
          <button
            className="btn btn-primary m-1 collapsed"
            data-toggle="collapse"
            data-target="#collapseTwo"
            aria-expanded="false"
            aria-controls="collapseTwo"
            onClick={this.handleForest}
          >
            Forecast
          </button>
          <button
            className="btn btn-primary m-1 collapsed"
            data-toggle="collapse"
            data-target="#collapseThree"
            aria-expanded="false"
            aria-controls="collapseThree"
          >
            Uvi
          </button>
        </div>
        <div
          id="collapseOne"
          className="row collapse show"
          data-parent="#accordion"
        >
          <Current current={this.state.current} />
        </div>
        <div
          id="collapseTwo"
          className=" row collapse"
          data-parent="#accordion"
        >
          <Forecast forest={this.state.forest} />
        </div>
        <div
          id="collapseThree"
          className="row collapse"
          data-parent="#accordion"
        >
          <Uvi />
        </div>
      </div>
    );
  }
}

export default Weather;

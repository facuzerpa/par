import React from "react";
import ForecastDay from "./ForecastDay";

class Forecast extends React.Component {
  constructor(props) {
    super(props);
    this.deleteToday = this.deleteToday.bind(this);
    this.splitInArray = this.splitInArray.bind(this);
    this.getMaxDay = this.getMaxDay.bind(this);
    this.listTemp_Max = this.listTemp_Max.bind(this);
    this.getMinDay = this.getMinDay.bind(this);
    this.listTemp_Min = this.listTemp_Min.bind(this);
    this.html = this.html.bind(this);
    this.getNameDay = this.getNameDay.bind(this);
    this.getNumberDay = this.getNumberDay.bind(this);
  }

  deleteToday(list) {
    if(list != null) {
      let toDay = new Date(list[0].dt_txt);
      const dataFilter = list.filter( weather => {
        var day = new Date(weather.dt_txt);
        if(day.getDate() != toDay.getDate()) return day;
      });
      return dataFilter;
    }
  }

  splitInArray(arr) {
    const newArray = [];
    while (arr.length > 0) {
      newArray.push(arr.splice(0, 8));
    }
    return newArray;
  }

  getMaxDay(array) {
    return Math.max.apply(Math, array.map(function(o) { return o.main.temp_max; }))
  }

  getMinDay(array) {
    return Math.min.apply(Math, array.map(function(o) { return o.main.temp_min; }))
  }

  listTemp_Max(array) {
    const newArray = [];
    newArray.push(array.map(obj => this.getMaxDay(obj)));
    return newArray;
  }

  listTemp_Min(array){
    const newArray = [];
    newArray.push(array.map(obj => this.getMinDay(obj)));
    return newArray;
  }

  getNameDay(fech) {
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    var date = new Date(fech);
    var nameDay = weekdays[date.getDay()];
    return nameDay;
  }

  getNumberDay(fech) {
    var date = new Date(fech);
    var fecha = date.getDate() + "/" + (date.getMonth() + 1);
    return fecha; 
  }

  html(list) {
    if(list != null) {
      console.log(list[0].weather[0].icon);
      var listMax = this.listTemp_Max(this.splitInArray(list));
      var listMin = this.listTemp_Min(this.splitInArray(list));
      var maxmin1 = listMax[0][0] + " °C "+ listMin[0][0];
      var maxmin2 = listMax[0][1] + " °C "+ listMin[0][1];
      var maxmin3 = listMax[0][2] + " °C "+ listMin[0][2];
      var maxmin4 = listMax[0][3] + " °C "+ listMin[0][3];
      var maxmin5 = listMax[0][4] + " °C "+ listMin[0][4];
      return (
        <div className="d-flex justify-content-center p-2 border border-primary w-100">
          <ForecastDay 
            imagen={list[0].weather[0].icon}
            diaNombre={this.getNameDay(list[0].dt_txt)} 
            fecha={this.getNumberDay(list[0].dt_txt)}
            weather={maxmin1}/>
          <ForecastDay 
            imagen={list[8].weather[0].icon}
            diaNombre={this.getNameDay(list[8].dt_txt)} 
            fecha={this.getNumberDay(list[8].dt_txt)}
            weather={maxmin2}/>
          <ForecastDay 
            imagen={list[16].weather[0].icon}
            diaNombre={this.getNameDay(list[16].dt_txt)} 
            fecha={this.getNumberDay(list[16].dt_txt)}
            weather={maxmin3}/>
          <ForecastDay 
            imagen={list[24].weather[0].icon}
            diaNombre={this.getNameDay(list[24].dt_txt)} 
            fecha={this.getNumberDay(list[24].dt_txt)}
            weather={maxmin4}/>
          <ForecastDay 
            imagen={list[32].weather[0].icon}
            diaNombre={this.getNameDay(list[32].dt_txt)} 
            fecha="5" 
            weather={maxmin5}/>
        </div>
      );
    } else {
      return <div><h1>Hola</h1></div>
    }
  }

  render() {
    return this.html(this.props.forest);
  }
}

export default Forecast;


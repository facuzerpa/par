import React from "react";
import {
  WiDaySunny,
  WiNightClear,
  WiDayCloudy,
  WiNightCloudy,
  WiCloud,
  WiCloudy,
  WiShowers,
  WiDayRain,
  WiNightRain,
  WiDayThunderstorm,
  WiNightThunderstorm,
  WiSnow,
  WiWindy
} from "react-icons/wi";

class Current extends React.Component {
  constructor(props) {
    super(props);
    this.image = this.image.bind(this);
    this.getHour = this.getHour.bind(this);
  }

  image(list) {
    if (list != null) {
      if (list.weather[0].icon == "01d") 
        return (
          <div align="center">
            <WiDaySunny size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "01n") 
        return (
          <div align="center">            
            <WiNightClear size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "02d") 
        return (
          <div align="center">
            <WiDayCloudy size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "02n") 
        return (
          <div align="center">
            <WiNightCloudy size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "03d" || list.weather[0].icon == "03n")
        return (
          <div align="center">
            <WiCloud size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "04d" || list.weather[0].icon == "04n")
        return (
          <div align="center">
            <WiCloudy size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "09d" || list.weather[0].icon == "09n")
        return (
          <div align="center">
            <WiShowers size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "10d") 
        return (
          <div align="center">
            <WiDayRain size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "10n") 
        return (
          <div align="center">
            <WiNightRain size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "11d")
        return (
          <div align="center">
            <WiDayThunderstorm size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "11n")
        return (
          <div align="center">
            <WiNightThunderstorm size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "13d" || list.weather[0].icon == "13n")
        return (
          <div align="center">
            <WiSnow size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
      if (list.weather[0].icon == "50d" || list.weather[0].icon == "50n")
        return (
          <div align="center">
            <WiWindy size={100} />
            <p>{list.weather[0].main}</p>
          </div>
        );
    }
  }

  getHour(time) {
    time = time - 10800;
    return new Date(time * 1000).toISOString().slice(-13, -8);
  }

  render() {
    return (
      <div className="d-flex justify-content-center p-2 border border-primary w-100">
        <div className="w-25 p-2 border border-primary">
            {this.image(this.props.current)}
        </div>
        <div className="w-25 p-2 border border-primary">
          <p>
            Preassure: {this.props.current != null ? this.props.current.main.pressure + " hpm": ""}
          </p>
          <p>
            Min Temp: {this.props.current != null ? this.props.current.main.temp_min + " C°": ""}
          </p>
          <p>
            Sunrise: {this.props.current != null ? this.getHour(this.props.current.sys.sunrise) : ""}
          </p>
          <p>
            Wind: {this.props.current != null ? this.props.current.wind.speed + " Km/h": ""}
          </p>
        </div>
        <div className="w-25 p-2 border border-primary">
          <p>
            Humidity: {this.props.current != null ? this.props.current.main.humidity + " %": ""}
          </p>
          <p>
            Max Temp: {this.props.current != null ? this.props.current.main.temp_max + " C°": ""}
          </p>
          <p>
            Sunset: {this.props.current != null ? this.getHour(this.props.current.sys.sunset) : ""}
          </p>
        </div>
      </div>
    );
  }
}

export default Current;

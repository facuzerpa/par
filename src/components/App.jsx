import React from "react";
import Header from "./Header";
import TitleForm from "./TitleForm";
import Weather from "./Weather";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: ""
    };
    this.handleUpdateLocation = this.handleUpdateLocation.bind(this);
  }

  handleUpdateLocation(arg) {
    this.setState({
      location: arg
    });
  }

  render() {
    return (
      <div>
        <Header />
        <TitleForm
          location={this.state.location}
          handleUpdateLocation={this.handleUpdateLocation}
        />
        <Weather location={this.state.location} />
      </div>
    );
  }
}

export default App;

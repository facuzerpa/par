import React from "react";
import Title from "./Title";
import Form from "./Form";

class TitleForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <div className="d-flex flex-column">
          <Title />
          <Form
            handleUpdateLocation={this.props.handleUpdateLocation}
            location={this.props.location}
          />
        </div>
      </div>
    );
  }
}

export default TitleForm;

import React from 'react';

class CurrentUvi extends React.Component {
    render() {
      return(
        <div className="row w-100 mt-5 m-0">
          <div className="d-flex align-items-center flex-column p-0 m-0 border border-primary w-100" >
            <div className="border border-primary w-100">
                <span>Current Uvi Index</span>
            </div>
            <div className="border border-primary w-100">
                <span>9.1</span>
            </div>
            <div className="border border-primary w-100">
                <span>Grafikito</span>
            </div>
          </div>
        </div>
      );
    }
  }
  
  export default CurrentUvi;
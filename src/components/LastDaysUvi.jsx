import React from 'react';

class LastDaysUvi extends React.Component {
    render() {
      return(
        <div className="row w-100 mt-5 m-0">
            <div className="border border-primary w-100">
                <span>Forecast Uvi Index:</span>
            </div>
            <div className="d-flex justify-content-center p-2 border border-primary w-100" >
                <span>Grafica</span>
            </div>
        </div>
      );
    }
  }
  
  export default LastDaysUvi;